package com.example.julian.jsoup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Handbook extends AppCompatActivity {
    private ListView ClassList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handbook);
        ClassList = findViewById(R.id.HandBookList);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(Handbook.this, R.layout.mylist, arrayList);
        ClassList.setAdapter(adapter);
    }

    public void ClickGenerate2(View view) {
        getWebsite();
    }

    private void getWebsite() {

        Thread downloadThread = new Thread() {

            public void run() {

                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect("http://www.handbook.unsw.edu.au/undergraduate/courses/2018/INFS1602.html").get();

                 //   Elements nodeBlogStats = doc.select("div[class=summary]");
                    Elements nodeBlogStats = doc.select("div.summary > p");

                    for (Element row : nodeBlogStats) {

                            System.out.println(row.text());
                        arrayList.add(row.text());

                    }


/*
                    for (int i = 0; i < elements.size(); i++) {
                       // Element row =  elements.get(i);
                        System.out.println(elements.text());
                      //  Elements cols = row.select("td");

                        //arrayList.add(cols.text());

                  }*/
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //result.setText(builder.toString());
                        adapter.notifyDataSetChanged();

                    }
                });
            }
        };
        downloadThread.start();
    }


}
