package com.example.julian.jsoup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private TextView result;
    private ListView ClassList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = (TextView) findViewById(R.id.worldtext);
        ClassList = findViewById(R.id.ClassView);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.mylist, arrayList);
        ClassList.setAdapter(adapter);


    }

    public void Handbook(View v){
        Intent intent = new Intent(MainActivity.this, Handbook.class);
        startActivity(intent);
    }


    public void ClickGenerate(View view) {
        getWebsite();
    }

    private void getWebsite() {

        Thread downloadThread = new Thread() {

            public void run() {

                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect("http://timetable.unsw.edu.au/2018/ECON1203.html").get();
                  //  Document doc = Jsoup.connect("http://timetable.unsw.edu.au/2018/INFS2603.html").get();

                    Elements table = doc.select("table");
                    Elements rows = table.select("tr");
                    //                    links.remove(0);
                //    builder.append(col.text()).append("\n");

                    for (int i = 0; i < rows.size(); i++) {
                        Element row =  rows.get(i);
                        Elements cols = row.select("td");
                     //   System.out.println(String.valueOf(i));

                       // System.out.println(cols.text());


                        if (cols.text().contains("Tutorial T2") || cols.text().contains("Lecture T2")) {
                          if (cols.text().length() < 100){
                              System.out.println(cols.text());
                              arrayList.add(cols.text());
                              //arrayList.add(cols.text().toString());
                              builder.append(cols.text());
                          }

                        }
                        /*
                        if (i == 21) {
                            System.out.println(String.valueOf(i));
                            System.out.println(cols.text());
                            builder.append(cols.text());
                        }*/

                        //
                    }
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //result.setText(builder.toString());
                        adapter.notifyDataSetChanged();

                    }
                });
            }
        };
        downloadThread.start();
    }
}
